import { Component, OnInit } from '@angular/core';


//Importar ruta activa 
import {ActivatedRoute} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';
@Component({
  selector: 'app-artista',
  templateUrl: './artista.component.html',
  styleUrls: ['./artista.component.css']
})
export class ArtistaComponent implements OnInit {


  artista: any = {};
  topTracks: any []=[];
  loadingArtista:boolean;

  constructor(private router: ActivatedRoute,
             private spotify:SpotifyService ) {

              this.loadingArtista=true;

this.router.params.subscribe(params =>{
//console.log(params['id']);
this.getArtista(params['id']);
this.getTopTracks(params['id']);

});

}


getArtista(id:string){

  this.spotify.getArtista(id).subscribe(artista =>{
    console.log(artista);
    this.artista=artista;

    this.loadingArtista=false;
  });
}

getTopTracks(id:string){

  this.spotify.getTopTracks(id)
  .subscribe(topTracks => {
      console.log('------------------------------------',topTracks);
      this.topTracks=topTracks;
  });
}

  ngOnInit(): void {
  }

}
