import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  paises :any[]=[];

  nuevasCanciones:any[]=[];
  loading :boolean;

  error:boolean =false;
  mensajeError:String;

  constructor(private  spotify:SpotifyService) { 
//this.http.get('https://restcountries.eu/rest/v2/lang/es').subscribe((respuesta:any) =>{
//this.paises=respuesta;});

    this.loading=true;
    this.error=false;

    this.spotify.getNeeReleases().subscribe((data:any) =>{
   //   console.log(data.albums.items);
   this.loading=false;
     this.nuevasCanciones=data;//.albums.items;
    
    },(errorServicio)=>{
      this.loading=false;
          this.error=true;
      console.log(errorServicio);
      this.mensajeError=errorServicio.error.error.message;
    });
  }

  ngOnInit(): void {
  }

}
