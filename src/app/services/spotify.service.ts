import { HttpClient,HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {map} from 'rxjs/operators';

@Injectable({
  //no es necesario declarar en el modulo si se encuentra este atributo
  providedIn: 'root'
})
export class SpotifyService {

  constructor(private http: HttpClient) { 
    console.log('servicio listo de spotify');
  }

  getQuery(query:String){
const url =`https://api.spotify.com/v1/${query}`;

const headers= new HttpHeaders({
  'Authorization':'Bearer BQDV46XsQno_EP49a01Fbylskuou4Fn9oYT3-7fq3qaubb_epBqz4-Rj6DwLgbzlWHJdpHZOAEHbiwHbfwo'
  });

  return this.http.get(url,{headers});
  }

  getNeeReleases(){

    return this.getQuery('browse/new-releases')
    .pipe(map(data =>data ['albums'].items));

//this.http.get('https://api.spotify.com/v1/browse/new-releases',{headers}).subscribe(data =>{
//  console.log(data);
//});
//return this.http.get('browse/new-releases',{headers}).pipe(map(data=>{
 // return data['albums'].items;///de busque una propiedad llamada album
//}));
  }
 

  getArtistas(termino:string){
    /*const headers= new HttpHeaders({
      'Authorization':'Bearer BQDdCXDQoOqUOxO3m7Xpx0QxYuupy3zFhEoJtJ0fCbmKTgdm_5rWsp958jMN_j3HQJAQRDQpbjJ2U1UQF0o'
      });
      return this.http.get(`https://api.spotify.com/v1/search?q=${termino}&type=artist&limit=15`,{headers}).pipe(map(data =>
         data['artists'].items));
*/
return this.getQuery(`search?q=${termino}&type=artist&limit=15`)
.pipe(map(data => data['artists'].items));
  }

  getArtista(id:string){
 
return this.getQuery(`artists/${id}`);
//.pipe(map(data => data['artists'].items));
  }

  getTopTracks(id:string){
      //return this.getQuery(`artists/${ id }/top-tracks`)
    //  console.log('*****************************************', data);
    return this.getQuery(`artists/${ id }/top-tracks?country=us`)
  
    .pipe(map(data => data['tracks']));

      }
  

}
